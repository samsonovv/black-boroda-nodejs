function sendRequest(event) {
    var data = JSON.stringify({ firstName: $('#firstName').val(), metro: $('#metro').val(), date: $('#date').val(), time: $('#time').val(), phone: $('#phone').val() });

    $.ajax({
        type: "POST",
        url: "users/order",
        data: data,
        contentType: "application/json",
        success: function () {
            alert("Данные сохранены");
            //$('#proceedSave').modal();
            console.log("Настройки успешно сохранены")
        },
        error: function () {
            alert("Ошибки при сохранении");
            //$('#proceedSave').modal();
            console.log("Ошибка")
        }
    });

}